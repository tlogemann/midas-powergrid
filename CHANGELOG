# Changelog

## Version 1.0.10 (tbd)

* [NEW] Adding the flag `double_actuator_values` to the grid params will double the maximum value for load and sgen actuators
  + If reactive power is negative, the minimum value will be doubled since the maximum value is zero.

## Version 1.0.9 (2022-08-11)

* [CHANGE] Removed the key value log at the beginning of a simulator step.

## Version 1.0.8 (2022-08-09)

* [FIX] Key Value logs now use `"` instead of `'` in the output.

## Version 1.0.7 (2022-07-13)

* [FIX] Actuator spaces were not defined correctly when negative reactive power was used
  + The assignment to `low` and `high` will now be swapped if necessary.

## Version 1.0.6 (2022-07-07)

* [FIX] Accessing the outputs was done using `iloc` which lead to errors when the grid entities index does not match the actual index of the data frame.

## Version 1.0.5 (2022-07-05)

* [FIX] Constrainted Grid: Power Flow calculations were not wrapped into try/except blocks
* [FIX] NaNs were not catched completely for mosaik.
  + Now every NaN that goes over mosaik should be replaced by 0. 

## Version 1.0.4 (2022-06-29)

* [NEW] Added bus voltage band constraint
* [FIX] The framework does actually work now
  + Tested with the midasmv

## Version 1.0.3 (2022-06-27)

* [FIX] Bugfixing the constraints framework
* [NEW] Added constant_sgen_p_mw to midasmv grid params

## Version 1.0.2 (2022-06-21)

* [FIX] Full report analysis failed with wrong key
* [CHANGE] `get_data` of `static.py` now returnes the actual values of `switches` instead of 0 in case the LF did not converge. 

## Version 1.0.1 (2022-06-15)

* [NEW] Added key-value log outputs for the pyrate project. 

## Version 1.0.0 (2022-06-01)

* [NEW] Powergrid `midasmv` takes now two optional key-value pairs as parameters
  + `constant_p_mw` will set all active power values to the value specified by this key.
  + `constant_q_mvar` likewise but for reactive power. 

## Version 1.0.0rc7 (2022-05-06)

* [NEW] Powergrid simulator can now be started as external process
  + Set `cmd: cmd` in *powergrid_params*
  + Powergrid will log to its own file *midas-powergrid.log*
* [NEW] The grid plotter is now part of this package

## Version 1.0.0rc6 (2022-05-03)

* [FIX] Constrainted grids were not loaded.

## Version 1.0.0rc5 (2022-05-03)

* [FIX] Added missing requirement

## Version 1.0.0rc4 (2022-05-03)

* [NEW] Added analysis function

## Version 1.0.0rc3 (2022-04-08)

* [NEW] Added the constrainted grid

## Version 1.0.0rc2 (2022-03-30)

* [CHANGE] Updated readme
* [NEW] Added gitlab-ci
* [NEW] Added tests      

## Version 1.0.0rc1 (2022-03-29)

* [NEW] Moved module to this new package
