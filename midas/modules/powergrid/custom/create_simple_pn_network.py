import pandapower as pp


def build_grid():
    net = pp.create_empty_network()
    b0 = pp.create_bus(net, vn_kv=110)
    b1 = pp.create_bus(net, vn_kv=20.)
    b2 = pp.create_bus(net, vn_kv=20.)
    b3 = pp.create_bus(net, vn_kv=20.)
    b4 = pp.create_bus(net, vn_kv=20.)

    pp.create_ext_grid(net, bus=b0)
    pp.create_transformer(net, hv_bus=b0, lv_bus=b1, std_type="63 MVA 110/20 kV")

    pp.create_line(net, from_bus=b1, to_bus=b2, length_km=2.5, std_type="NAYY 4x50 SE")
    pp.create_line(net, from_bus=b2, to_bus=b3, length_km=2.5, std_type="NAYY 4x50 SE")
    pp.create_line(net, from_bus=b3, to_bus=b4, length_km=2.5, std_type="NAYY 4x50 SE")

    #pp.create_gen(net, bus=b2, p_mw=0.)
    pp.create_sgen(
        net,
        bus=b2,
        p_mw=0.0,
        q_mvar=0.0,
        name="SGEN_{}".format(b2),
        scaling=1.0,
        type=None,
        in_service=True,
        controllable=False,
    )
    pp.create_load(net, bus=b3, p_mw=1.)
    pp.create_load(net, bus=b4, p_mw=1.)

    pp.runpp(net)

    print(net.res_bus.vm_pu)
    print(net.res_line.loading_percent)

    return net
